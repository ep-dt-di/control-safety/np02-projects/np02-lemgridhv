main()
{   
  float fChActualVMon, fChSettingsV;
  bool  OVC;  
  string d_sChList, d_sChNomList;
  string value;
  
  
  d_sChList = dpNames("dist_1:CAEN/CAENLAB/board00/channel003", "FwCaenChannelA1580");  
  d_sChNomList = dpNames("dist_1:Board*Channel003", "CaenChannelConfig");
  
//  dpGet(d_sChList + ".actual.vMon", fChActualVMon); 
//  dpGet(d_sChNomList + ".settings.v0", fChSettingsV);
  
//  dpSet(d_sChList + ".actual.vMon", fChSettingsV);
  
  
  do
  {
   
   dpGet( d_sChList + ".settings.v0", fChSettingsV);
   dpGet( d_sChList + ".actual.vMon", fChActualVMon);

   if((fChSettingsV - fChActualVMon) > 30.0)
   {
      dpSet(d_sChList + ".actual.vMon", fChActualVMon + 30.0);
   }
   if((fChSettingsV - fChActualVMon) <= 30.0 && (fChSettingsV - fChActualVMon) > 0.0)
   {
      dpSet(d_sChList + ".actual.vMon", fChSettingsV);
   }
   if (fChSettingsV == fChActualVMon)
   {
      DebugN("We have completed the ramp up OR we were already at the nominal values");
   }
    
   delay(20);
      
  
   DebugN("WE ARE GETTING IN OVC FOR 10 SECONDS-->");
   dpSet(d_sChList + ".actual.OvC", TRUE);
   dpGet(d_sChList + ".actual.vMon", fChActualVMon);
   dpSet(d_sChList + ".actual.vMon", fChActualVMon - 180.0);
   delay(10);
   dpSet(d_sChList + ".actual.OvC", FALSE);
   dpGet( d_sChList + ".settings.v0", fChSettingsV);
   dpGet( d_sChList + ".actual.vMon", fChActualVMon);

   if((fChSettingsV - fChActualVMon) > 30.0)
   {
      dpSet(d_sChList + ".actual.vMon", fChActualVMon + 30.0);
   }
   if((fChSettingsV - fChActualVMon) <= 30.0 && (fChSettingsV - fChActualVMon) > 0.0)
   {
      dpSet(d_sChList + ".actual.vMon", fChSettingsV);
   }
   if (fChSettingsV == fChActualVMon)
   {
      DebugN("We have completed the ramp up OR we were already at the nominal values");
   }
    
   delay(120);
   

   DebugN("WE ARE GETTING IN OVC FOR 15 SECONDS--> ");
   dpSet(d_sChList + ".actual.OvC", TRUE);
   dpGet(d_sChList + ".actual.vMon", fChActualVMon);
   dpSet(d_sChList + ".actual.vMon", fChActualVMon - 190.0);
   delay(15);
   dpSet(d_sChList + ".actual.OvC", FALSE);
   dpGet( d_sChList + ".settings.v0", fChSettingsV);
   dpGet( d_sChList + ".actual.vMon", fChActualVMon);

   if((fChSettingsV - fChActualVMon) > 30.0)
   {
      dpSet(d_sChList + ".actual.vMon", fChActualVMon + 30.0);
   }
   if((fChSettingsV - fChActualVMon) <= 30.0 && (fChSettingsV - fChActualVMon) > 0.0)
   {
      dpSet(d_sChList + ".actual.vMon", fChSettingsV);
   }
   if (fChSettingsV == fChActualVMon)
   {
      DebugN("We have completed the ramp up OR we were already at the nominal values");
   }
      
   delay(120);
   

   DebugN("WE ARE GETTING IN OVC FOR 22 SECONDS--> ");
   dpSet(d_sChList + ".actual.OvC", TRUE); 
   dpGet(d_sChList + ".actual.vMon", fChActualVMon);
   dpSet(d_sChList + ".actual.vMon", fChActualVMon - 100.0);
   delay(22);
   dpSet(d_sChList + ".actual.OvC", FALSE);
   dpGet( d_sChList + ".settings.v0", fChSettingsV);
   dpGet( d_sChList + ".actual.vMon", fChActualVMon);

   if((fChSettingsV - fChActualVMon) > 30.0)
   {
      dpSet(d_sChList + ".actual.vMon", fChActualVMon + 30.0);
   }
   if((fChSettingsV - fChActualVMon) <= 30.0 && (fChSettingsV - fChActualVMon) > 0.0)
   {
      dpSet(d_sChList + ".actual.vMon", fChSettingsV);
   }
   if (fChSettingsV == fChActualVMon)
   {
      DebugN("We have completed the ramp up OR we were already at the nominal values");
   }
      
   delay(120);
   
   DebugN("WE ARE GETTING IN OVC FOR 40 SECONDS: WARNING: TRIP SITUATION--> ");
   dpSet(d_sChList + ".actual.OvC", TRUE);
   dpGet(d_sChList + ".actual.vMon", fChActualVMon);
   dpSet(d_sChList + ".actual.vMon", fChActualVMon - 300.0);
   delay(40);  
   dpSet(d_sChList + ".actual.OvC", FALSE);
      
   dpGet( d_sChList + ".settings.v0", fChSettingsV);
   dpGet( d_sChList + ".actual.vMon", fChActualVMon);

   if((fChSettingsV - fChActualVMon) > 30.0)
   {
     dpSet(d_sChList + ".actual.vMon", fChActualVMon + 30.0);
    }
   if((fChSettingsV - fChActualVMon) <= 30.0 && (fChSettingsV - fChActualVMon) > 0.0)
   {
     dpSet(d_sChList + ".actual.vMon", fChSettingsV);
   }
   if (fChSettingsV == fChActualVMon)
   {
     DebugN("We have completed the ramp up OR we were already at the nominal values");
   }
    
   delay(60);
   
 }while (1);  
}  
