main()
{
  bool bOverCurrentSts, bOverCurrentMem, end = true;
  time tStartRecover = getCurrentTime(), tTimeWOutOVC;
  float fChNominalVal, fChActualSP, fChSettings, fChTripTime, fChTripPanic=5.0;
  dyn_int d_intChCount, d_timecounter; 
  dyn_string d_sChList, d_sChNomList;
  dyn_time d_tStartRecover;
  dyn_bool d_bOverCurrentMem;
  int iChNumber, i;

  //channel list to control      
//  d_sChList = dpNames("dist_1:CAEN/CAENLAB/board*/channel*", "FwCaenChannelA1580");
//  d_sChNomList = dpNames("dist_1:Board*Channel*", "CaenChannelConfig");
  d_sChList = "dist_1:CAEN/CAENLAB/board00/channel003";
  d_sChNomList = "dist_1:Board00Channel003";
  //DebugN("list of channels -> ", d_sChList);
  iChNumber = dynlen(d_sChList);
  for (i=1; i<=iChNumber; i++)
  {
   // datapoint element to consider -> "System1:CAEN/CAENLAB/board00/channel000.actual.OvC"
   dpConnect("CaenChOvcCtrl", d_sChList[i] + ".actual.OvC"); // + datapoint from run control...
   dynAppend(d_tStartRecover, getCurrentTime());
   dynAppend(d_bOverCurrentMem, false);
   dynAppend(d_intChCount, 0);
   dynAppend(d_timecounter, 0);
  }
  do
  {
    //test Run control status 
    for (i=1; i < iChNumber; i++)
    {    
      //test if nominal voltage or channel OFF        
      dpGet(d_sChList[i] + ".actual.OvC", bOverCurrentSts);
 
      if (bOverCurrentSts)
      {
         // OVC mode
        //d_tStartRecover[i] = getCurrentTime();
        d_bOverCurrentMem[i] = true;
        dpGet(d_sChList[i] + ".settings.tripTime", fChTripTime);
        if (d_timecounter[i] == (fChTripTime - fChTripPanic)){
          DebugN("SITUATION OF TRIP MODE--> ", d_timecounter[i], i);
          dpGet(d_sChList[i] + ".settings.v0", fChSettings);
          dpSet(d_sChList[i] + ".settings.v0", fChSettings - 50.0);
          
                 
        }
        d_timecounter[i] = d_timecounter[i] + 1;
      }
      else
      {  
        if (!bOverCurrentSts && d_bOverCurrentMem[i])
        {
          // Recovering after an OVC 
          d_timecounter[i] = 0;
          d_tStartRecover[i] = getCurrentTime();
          d_bOverCurrentMem[i] = false;
        }
        else
        {
          // Normal status, no OVC
          d_bOverCurrentMem[i] = false;
          d_timecounter[i] = 0;
          tTimeWOutOVC = getCurrentTime() - d_tStartRecover[i];
          d_intChCount[i]  = minute(tTimeWOutOVC);        
//          DebugN("Time without over current -> ", d_intChCount[i], minute(tTimeWOutOVC));
          if( d_intChCount[i] >= 20)//20  min without OVC
          {
            //Adjust current by step to nominal
            dpGet(d_sChNomList[i] + ".NominalValue", fChNominalVal, 
                  d_sChList[i] + ".settings.v0", fChActualSP);
            if((fChNominalVal - fChActualSP) > 10.0)
            {
              dpSet(d_sChList[i] + ".settings.v0", fChActualSP + 10.0);
            //  DebugN(d_sChList[i]," -> Increase Voltage to ", fChActualSP + 10.0);
            }
            if((fChNominalVal - fChActualSP) <= 10.0 && (fChNominalVal - fChActualSP) > 0.0)
            {
              dpSet(d_sChList[i] + ".settings.v0", fChNominalVal);
              //DebugN(d_sChList[i]," -> Increase Voltage to ", fChNominalVal);
            }
            if (fChNominalVal == fChActualSP)
            {
              //DebugN(d_sChList[i]," -> Voltage already at nominal");
            }
            d_tStartRecover[i] = getCurrentTime();
          }
        }
      }
    }
    //DebugN("Time without over current -> ", d_intChCount[1], d_intChCount[2], d_intChCount[3], d_intChCount[4], d_intChCount[5], d_intChCount[6], d_intChCount[7], d_intChCount[8], d_intChCount[9], d_intChCount[10], d_intChCount[11]);
    delay(1);
  }
  while(end);
}

CaenChOvcCtrl(string dp1, bool bIntlk)
{
  dyn_bool dbIntlk1, dbIntlk2, dbIntlk3;
  dyn_time dtIntlk1, dtIntlk2, dtIntlk3;
  dyn_string sDp1Split;
  string sDp;
  time tEnd = getCurrentTime(), tStart1, tStart2, tStart3;
  int iStartPeriod, iCount = 0;
  int iC1 = 0, iC2 = 0, iC3 = 0;
  int iError1, iLength1, iError2, iLength2, iError3, iLength3; 
  int iPeriod1 = 180, iPeriod2=600, iPeriod3 = 86400; // iPeriod3 corresponds to 1 day
  float fOvcRate;

  float fActualSP;
    
  iStartPeriod = period(tEnd);
  setPeriod(tStart1, (iStartPeriod - iPeriod1));
//  setPeriod(tStart2, (iStartPeriod - iPeriod2));
//  setPeriod(tStart3, (iStartPeriod - iPeriod3));
  
  iError1 = dpGetPeriod (tStart1, tEnd, iCount, dp1, dbIntlk1, dtIntlk1);
//  iError2 = dpGetPeriod (tStart2, tEnd, iCount, dp1, dbIntlk2, dtIntlk2);
//  iError3 = dpGetPeriod (tStart3, tEnd, iCount, dp1, dbIntlk3, dtIntlk3);
  
    
  iLength1 = dynlen(dbIntlk1);
//  iLength2 = dynlen(dbIntlk2);
//  iLength3 = dynlen(dbIntlk3);
  
//  DebugN("pppppppppppppppppppppppppppppppppppppppppppppppp", dp1, bIntlk, iLength1); 
    
  if((iLength3 != 0) && ( iError3 != -1))
  {
    for(int k=1 ; k <= iLength3 ; k=k+1)
    {
      if (dbIntlk3[k])
      {
        iC3 = iC3 + 1;
      }
    }
  }
  
    
  
  
  fOvcRate = iC3/24.;

    DebugN("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPpp ", fOvcRate);
  
  sDp1Split = strsplit(dp1, ".");
  sDp = sDp1Split[1] + ".actual.OvCRate";
  dpSet(sDp, fOvcRate);
  

  
  if((iLength1 != 0) && ( iError1 != -1))
  {
    for(int i=1 ; i <= iLength1 ; i=i+1)
    {
      if (dbIntlk1[i])
      {
        iC1 = iC1 + 1;
      }
    }
    DebugTN("Number of OVC in interval 1 for ", dp1," -> ", iC1);
    //adjust current if iC1 > 2
    if((iC1 >= 2) &&  bIntlk)
    {
      //search dpName and add property to set 
      //"System1:CAEN/CAENLAB/board00/channel000.settings.v0"
      sDp1Split = strsplit(dp1, ".");
      sDp = sDp1Split[1] + ".settings.v0";
      //lower V0Set by 50 V      
      dpGet(sDp, fActualSP);
      //DebugN("lowering channel ", sDp , " to ", fActualSP - 50.0);
      dpSet(sDp, fActualSP - 50.0);
    }
  }
  else
  {
    if(iLength1 == 0)
    {
      DebugN(dp1, " -> No Overcurrent in C1");
    }
    else
    {
      DebugN(dp1, " -> Connexion problem with database for C1");
    }
  }

  if((iLength2 != 0) && ( iError2 != -1))
  {
    for(int j=1 ; j <= iLength2 ; j=j+1)
    {
      if (dbIntlk2[j])
      {
        iC2 = iC2 + 1;
      }
    }
    DebugTN("Number of OVC in interval 2 for ", dp1," -> ", iC2);

    if((iC2 >= 4 && iC1>=2) &&  bIntlk)
    {
      sDp1Split = strsplit(dp1, ".");
      sDp = sDp1Split[1] + ".settings.v0";
      //lower V0Set by 50 V      
      dpGet(sDp, fActualSP);
      //DebugN("lowering channel ", sDp , " to ", fActualSP - 50.0);
      dpSet(sDp, fActualSP - 50.0);
    }
  }
  else
  {
    if(iLength2 == 0 && iLength1 == 0)
    {
      DebugN(dp1, " -> No Overcurrent in C1 nor in C2");
    }
    else
    {
      DebugN(dp1, " -> Connexion problem with database for C1 or C2");
    }
  }
  
}
