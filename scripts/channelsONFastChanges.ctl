main()
{   
  float fChActualVMon, fChSettingsV;
  int randvalue, iChNumber, i;
  dyn_string d_sChList;
  string value;
  
  
//  d_sChList = dpNames("dist_1:CAEN/CAENLAB/board*/channel*", "FwCaenChannelA1580");
  d_sChList = makeDynString("dist_1:CAEN/CAENLAB/board00/channel000","dist_1:CAEN/CAENLAB/board00/channel001");
  iChNumber = dynlen(d_sChList);

  for (i=1; i<=iChNumber; i++)
  {
    dpGet(d_sChList[i] + ".settings.v0", fChSettingsV);
    dpSet(d_sChList[i] + ".actual.vMon", fChSettingsV);
    dpGet(d_sChList[i] + ".actual.vMon", fChActualVMon);
  }
  
  do
  {
   delay(20);

   for (i=1; i<=iChNumber; i++){  
 
        DebugN("WE ARE GETTING IN OVC");
        dpSet(d_sChList[i] + ".actual.OvC", TRUE);
        dpSet(d_sChList[i] + ".actual.vMon", fChActualVMon - 80.0);
        delay(25);  
        dpSet(d_sChList[i] + ".actual.vMon", fChSettingsV);
        dpSet(d_sChList[i] + ".actual.OvC", FALSE);
   }

 }while(1);
    
  
}
