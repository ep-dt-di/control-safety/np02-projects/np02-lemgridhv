main()
{
  
  dyn_bool dbIntlk;
  dyn_time dtIntlk;
  time tEnd = getCurrentTime(), tStart;
  int iStartPeriod, iCount = 0;
  int iError, iLength; 
  int iPeriod = 180; 
  string dp = "dist_1:CAEN/CAENLAB/board00/channel009.actual.OvC";
  
      
  iStartPeriod = period(tEnd);
  setPeriod(tStart, (iStartPeriod - iPeriod));
  
  
  iError = dpGetPeriod (tStart, tEnd, iCount, dp, dbIntlk, dtIntlk);    
  iLength = dynlen(dbIntlk);
 
 if (iError == -1 || iLength == 0) /* Is executed if a query error occurs or no values exist */
  {  
  DebugN("dpGetPeriod caused an error or no values exist");
   }
 else
  {
  int i; //loop variable
  DebugN("Result values:");
  for(i=1;i<=iLength;i++)
  DebugN(dbIntlk[i],dtIntlk[i], tStart, iStartPeriod);
  }
 
  
}
