main()
{   
  float fChActualVMon, fChSettingsV;
  int randvalue, iChNumber, i;
  dyn_string d_sChList;
  string value;
  
  
//  d_sChList = dpNames("dist_1:CAEN/CAENLAB/board*/channel*", "FwCaenChannelA1580");
  d_sChList = makeDynString("dist_1:CAEN/CAENLAB/board00/channel000", "dist_1:CAEN/CAENLAB/board00/channel001");
  iChNumber = dynlen(d_sChList);

  for (i=1; i<=iChNumber; i++)
  {
    dpGet(d_sChList[i] + ".settings.v0", fChSettingsV);
    dpSet(d_sChList[i] + ".actual.vMon", fChSettingsV);
  }
  
  do
  {
   delay(1);

   for (i=1; i<=iChNumber; i++){  
   randvalue = rand(); 
  
//   if(randvalue>=15000 && randvalue<=15060){
//        DebugN("This is the value of the rand value in 10 seconds OVC MODE",randvalue);
//        DebugN("WE ARE GETTING IN OVC FOR 10 SECONDS--> ", randvalue, d_sChList[i]);
//        dpSet(d_sChList[i] + ".actual.OvC", TRUE);
//        dpSet(d_sChList[i] + ".actual.vMon", fChActualVMon[i] - 380.0);
//        delay(10);
//        dpSet(d_sChList[i] + ".actual.OvC", FALSE);
//   }
   
//   if(randvalue>=15152 && randvalue<=15641){
   
//        DebugN("This is the value of the rand value in 15 seconds OVC mode",randvalue);
//        DebugN("WE ARE GETTING IN OVC FOR 15 SECONDS--> ", randvalue, d_sChList[i]);
//        dpSet(d_sChList[i] + ".actual.OvC", TRUE); 
//        dpSet(d_sChList[i] + ".actual.vMon", fChActualVMon[i] - 300.0);
//        delay(15);
//        dpSet(d_sChList[i] + ".actual.OvC", FALSE);
//   }
   DebugN("This is the rand value   ", randvalue);
   if(randvalue>=15000 && randvalue<=17000){
        DebugN("WE ARE GETTING IN OVC --> ", randvalue, d_sChList[i]);
        dpSet(d_sChList[i] + ".actual.OvC", TRUE);
        dpSet(d_sChList[i] + ".actual.vMon", fChActualVMon - 300.0);
        delay(70);  
//        dpSet(d_sChList[i] + ".actual.OvC", FALSE);
   }
   
   
     dpGet( d_sChList[i] + ".settings.v0", fChSettingsV);
     dpGet( d_sChList[i] + ".actual.vMon", fChActualVMon);
      
     
     if((fChSettingsV - fChActualVMon) > 10.0){
       dpSet(d_sChList[i] + ".actual.vMon", fChActualVMon + 10.0);
     }
      
     if((fChSettingsV - fChActualVMon) <= 10.0 && (fChSettingsV - fChActualVMon) > 0.0)
     {
       dpSet(d_sChList[i] + ".actual.vMon", fChSettingsV);
     }
       
     if (fChSettingsV == fChActualVMon){
       dpSet(d_sChList[i] + ".actual.OvC", FALSE);}  
   }

 }while(1);
    
  
}
