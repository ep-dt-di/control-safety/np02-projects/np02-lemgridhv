main()
{
  bool bOverCurrentSts, bOverCurrentMem, end = true;
  time d_tStartRecover = getCurrentTime(), tTimeWOutOVC;
  float fChNominalVal, fChTopActualSP, fChTopNominalValue, fChActualSP, fChSettings, fChTripTime, fChTripPanic, fChv0Decrease, fChv0Step;
  int fChGroup, fChTopGroup, fChBottonGroup, fChElapsedTime;
  string fChCRP, fChTopCRP, fChBottonCRP, fChPosition;
  dyn_int d_intChCount, d_intChCountSec, d_timecounter; 
  bool fChTopSelection, fChBottonSelection, fChtrip, fChC1, fChC2, fChOn, fChTopOn, tripstatus=false;
  dyn_string d_sChList, d_sChNomList, d_sChrampList;
  dyn_time d_tStartRecover;
  dyn_bool d_bOverCurrentMem;
  int iChNumber, i, m, s, tmpcounter = 0;

  //channel list to control      
  d_sChList = dpNames("NP02_DCS_01:CAEN/NP02-DCS-PS-LEM-01/board*/channel*", "FwCaenChannelA1580");
  d_sChNomList = dpNames("NP02_DCS_01:Board*Channel*", "CaenChannelConfig");
  d_sChrampList = dpNames("NP02_DCS_01:board*_channel*", "NP02_CaenChannelA1580");
  
  iChNumber = dynlen(d_sChList);
  for (i=1; i<=iChNumber; i++)
  {
   // datapoint element to consider -> "System1:CAEN/NP02-DCS-PS-LEM-01/board00/channel000.actual.OvC"
   dpConnect("CaenChOvcCtrl", d_sChList[i] + ".actual.OvC"); // + datapoint from run control...     
   dpConnect("CaenIsOnCtrl", d_sChList[i] + ".actual.isOn"); // + datapoint from run control... 
   dynAppend(d_tStartRecover, getCurrentTime());
   dynAppend(d_bOverCurrentMem, false);
   dynAppend(d_intChCount, 0);
   dynAppend(d_timecounter, 0);
  }
  do
  {
    tmpcounter = tmpcounter + 1;
    //test Run control status 
    for (i=1; i <= iChNumber; i++)
    {    
            
      dpGet(d_sChList[i] + ".actual.OvC", bOverCurrentSts);
      dpGet(d_sChList[i] + ".settings.tripTime", fChTripTime);
      dpGet(d_sChrampList[i] + ".settings.tripPanic", fChTripPanic);
      dpGet(d_sChrampList[i] + ".settings.v0Decrease", fChv0Decrease);
      dpGet(d_sChrampList[i] + ".settings.v0Step", fChv0Step);
      dpGet(d_sChNomList[i] + ".TopSelection", fChTopSelection);

      
      if (bOverCurrentSts) // OVC Mode
      {
 
        d_bOverCurrentMem[i] = true;
        
        d_timecounter[i] = d_timecounter[i] + 1;
        
        if (d_timecounter[i] == (fChTripTime - fChTripPanic)){
          tripstatus=true;
          DebugN("TRIP STATUS mode", tripstatus);
        } 
        else
        {
          tripstatus = false;
        }
        
        if (tripstatus){
          dpGet(d_sChList[i] + ".settings.v0", fChSettings);
          dpSet(d_sChList[i] + ".settings.v0", fChSettings - fChv0Decrease);
          d_timecounter[i] = 0;
         
          if (fChTopSelection){
          dpGet(d_sChNomList[i] + ".Group", fChTopGroup);
          dpGet(d_sChNomList[i] + ".ChargeReadoutPlane", fChTopCRP);
          
          for (m=1; m<=iChNumber; m++){
              dpGet(d_sChNomList[m] + ".ChargeReadoutPlane", fChCRP);
              dpGet(d_sChNomList[m] + ".Group", fChGroup);
              dpGet(d_sChNomList[m] + ".Position", fChPosition);
              if(fChCRP == fChTopCRP && fChGroup == fChTopGroup && fChPosition == "Bottom"){
                dpGet(d_sChList[m] + ".settings.v0", fChSettings);
                dpGet(d_sChList[m] + ".actual.isOn", fChOn);
                if (fChOn){
                dpSet(d_sChList[m] + ".settings.v0", (fChSettings - fChv0Decrease));
                }
              }
            }
        }// end of TopSelection condition 
        } // end of tripstatus condition   
      }// end of OVC condition
      else
      {  
               
        if (!bOverCurrentSts && d_bOverCurrentMem[i])
        {
          // Recovering after an OVC 
          d_timecounter[i] = 0;
          d_tStartRecover[i] = getCurrentTime();
          d_bOverCurrentMem[i] = false;
        }
        else
        {

          // Normal status, no OVC --> Starting to ramp up
          d_bOverCurrentMem[i] = false;
          d_timecounter[i] = 0;
          tTimeWOutOVC = getCurrentTime() - d_tStartRecover[i];
          d_intChCount[i]  = minute(tTimeWOutOVC); 
          d_intChCountSec[i]  = second(tTimeWOutOVC);
          dpGet(d_sChrampList[i] + ".settings.ElapsedTime", fChElapsedTime);   

          if( d_intChCount[i] >= fChElapsedTime)//time in min without OVC
          {
            dpGet(d_sChNomList[i] + ".NominalValue", fChNominalVal, 
                  d_sChList[i] + ".settings.v0", fChActualSP,
                  d_sChNomList[i] + ".BottonSelection", fChBottonSelection,
                  d_sChNomList[i] + ".TopSelection", fChTopSelection);
// Starting the TOP selection                  
            if (fChTopSelection){
//              DebugN("TOP and the values are   ", d_sChList[i], fChNominalVal, fChActualSP);
               dpGet(d_sChList[i] + ".actual. ", fChOn);
               if((fChNominalVal - fChActualSP) > fChv0Step && fChOn)
               {
                 dpSet(d_sChList[i] + ".settings.v0", fChActualSP + fChv0Step);
               }
               if((fChNominalVal - fChActualSP) <= fChv0Step && (fChNominalVal - fChActualSP) > 0.0 && fChOn)
               {
                 dpSet(d_sChList[i] + ".settings.v0", fChNominalVal);
               }
            }
// Finishing the TOP selection
// Starting the BOT Selection
            
            else if (fChBottonSelection){
              dpGet(d_sChNomList[i] + ".ChargeReadoutPlane", fChBottonCRP,
                  d_sChNomList[i] + ".Group", fChBottonGroup,
                  d_sChList[i] + ".actual.isOn", fChOn);
              for (s=1; s<=iChNumber; s++)
              {
              dpGet(d_sChNomList[s] + ".ChargeReadoutPlane", fChCRP);
              dpGet(d_sChNomList[s] + ".Group", fChGroup);
              dpGet(d_sChNomList[s] + ".Position", fChPosition);
                 if(fChCRP == fChBottonCRP && fChGroup == fChBottonGroup && fChPosition == "Top")
                 {
                       dpGet(d_sChList[s] + ".settings.v0", fChTopActualSP);
                       dpGet(d_sChList[s] + ".actual.isOn", fChTopOn);
                       dpGet(d_sChNomList[s] + ".NominalValue", fChTopNominalValue);

                     if ((fChActualSP - fChTopActualSP)<= (fChNominalVal-fChTopNominalValue))
                    {
                     if((fChNominalVal - fChActualSP) > fChv0Step && fChOn && fChTopOn)
                     {
                       dpSet(d_sChList[i] + ".settings.v0", fChActualSP + fChv0Step);
                     }
                    if((fChNominalVal - fChActualSP) <= fChv0Step && (fChNominalVal - fChActualSP) > 0.0 && fChOn && fChTopOn)
                    {
                      dpSet(d_sChList[i] + ".settings.v0", fChNominalVal);
                    }
                
                   }
                }
              }              
            }
            d_tStartRecover[i] = getCurrentTime();

          }
        }
        
        
      }
    }

    delay(1);
  }
  while(end);
}





CaenChOvcCtrl(string dp1, bool bIntlk)
{

  dyn_bool dbIntlk1, dbIntlk2, dbIntlk3;
  dyn_time dtIntlk1, dtIntlk2, dtIntlk3;
  dyn_string sDp1Split, sDp2Split, sDp3Split,sDp4Split, sDp5Split, sDp6Split, all_channels, all_nomchannels;
  string sDpC1, sDpC2, sDpRate, sDpPosition, sDpGroup, sDpCRP, sDpC1N, sDpC2N, sDpC1T, sDpC2T;
  time tEnd = getCurrentTime(), tStart1, tStart2, tStart3;
  int iStartPeriod, iCount = 0;
  int iC1 = 0, iC2 = 0, iC3 = 0;
  int iError1, iLength1, iError2, iLength2, iError3, iLength3; 
  int iPeriod1, iPeriod2, iPeriod3 = 86400; // iPeriod3 corresponds to 1 day
  float fOvcRate, fActualV0, fsettingV0, fSettingsv0Decrease;
  bool dpC1, dpC2, fActualOn;
  int nch, j, C1N, C2N;
  string position, refposition, crp, refcrp;
  int group, refgroup;

  all_channels = dpNames("NP02_DCS_01:CAEN/NP02-DCS-PS-LEM-01/board*/channel*", "FwCaenChannelA1580");
  all_nomchannels = dpNames("NP02_DCS_01:Board*Channel*", "CaenChannelConfig");
  
  nch = dynlen(all_channels);
  
  sDp1Split = strsplit(dp1, ".");
  sDp2Split = strsplit(dp1, "/");
  sDp3Split = strsplit(sDp2Split[4], ".");
  
  
  sDp4Split = strsplit(sDp1Split[1], "/");
  sDp5Split = strsplit(sDp4Split[3],"d");
  sDp6Split = strsplit(sDp4Split[4],"l");
                  
  ///"NP02_DCS_01:board00_channel000.actual.OvCRate"
  sDpRate = "NP02_DCS_01:" + sDp2Split[3] +"_" + sDp3Split[1] +".actual.OvCRate";
  sDpC1 =  "NP02_DCS_01:" + sDp2Split[3] +"_" + sDp3Split[1]+ ".actual.C1";
  sDpC2 = "NP02_DCS_01:" + sDp2Split[3] +"_" + sDp3Split[1] + ".actual.C2";
  sDpC1N = "NP02_DCS_01:" + sDp2Split[3] +"_" + sDp3Split[1] + ".actual.C1Number";
  sDpC2N = "NP02_DCS_01:" + sDp2Split[3] +"_" + sDp3Split[1] + ".actual.C2Number"; 
  sDpC1T = "NP02_DCS_01:" + sDp2Split[3] +"_" + sDp3Split[1] + ".actual.C1Time";
  sDpC2T = "NP02_DCS_01:" + sDp2Split[3] +"_" + sDp3Split[1] + ".actual.C2Time"; 
  sDpPosition = "NP02_DCS_01:" + "Board" + sDp5Split[2] + "Channel" + sDp6Split[2] + ".Position";
  sDpGroup = "NP02_DCS_01:" + "Board" + sDp5Split[2] + "Channel" + sDp6Split[2] + ".Group";
  sDpCRP = "NP02_DCS_01:" + "Board" + sDp5Split[2] + "Channel" + sDp6Split[2] + ".ChargeReadoutPlane";
  
  iStartPeriod = period(tEnd);
  dpGet(sDpC1T, iPeriod1);
  dpGet(sDpC2T, iPeriod2);
  setPeriod(tStart1, (iStartPeriod - iPeriod1));
  setPeriod(tStart2, (iStartPeriod - iPeriod2));
  setPeriod(tStart3, (iStartPeriod - iPeriod3));
    
  
  iError1 = dpGetPeriod (tStart1, tEnd, iCount, dp1, dbIntlk1, dtIntlk1);
  iError2 = dpGetPeriod (tStart2, tEnd, iCount, dp1, dbIntlk2, dtIntlk2);
  iError3 = dpGetPeriod (tStart3, tEnd, iCount, dp1, dbIntlk3, dtIntlk3);
    
  iLength1 = dynlen(dbIntlk1);
  iLength2 = dynlen(dbIntlk2);
  iLength3 = dynlen(dbIntlk3);
  
// Condition 1    

  dpGet(sDpC1N, C1N);
 
  if((iLength1 != 0) && ( iError1 != -1))
  {
      for(int t=1 ; t <= iLength1 ; t=t+1)
      {
        if (dbIntlk1[t])
        {
          iC1 = iC1 + 1;
        }
      }
      DebugN("Number of OVC in interval 1 for ", dp1," -> ", iC1);
      if((iC1 >= C1N) &&  bIntlk)
      {   
        dpSet(sDpC1,TRUE);
      }
      if((iC1<C1N) || !bIntlk){
        dpSet(sDpC1,FALSE);
      }
  }
  else
  {
    if(iLength1 == 0)
    {
      dpSet(sDpC1,FALSE);
    }
    else
    {
      DebugN(dp1, " -> Connexion problem with database for C1");
    }
  }
  
  
// Condition 2
  dpGet(sDpC2N, C2N);
  if((iLength2 != 0) && ( iError2 != -1))
  {
    for(int j=1 ; j <= iLength2 ; j=j+1)
    {
      if (dbIntlk2[j])
      {
        iC2 = iC2 + 1;
      }
    }
    DebugN("Number of OVC in interval 2 for ", dp1," -> ", iC2);
    if((iC2 >= C2N) &&  bIntlk)
    { 
      dpSet(sDpC2,TRUE);
    }
    if((iC2<C2N) || !bIntlk){
      dpSet(sDpC2,FALSE);
    }
  }
  else
  {
    if(iLength2 == 0)
    {
       dpSet(sDpC2,FALSE);
    }
    else
    {
      DebugN(dp1, " -> Connexion problem with database for C2");
    }
  }
    
 
  dpGet(sDpC1,dpC1);
  dpGet(sDpC2,dpC2);  
  dpGet(sDpPosition, refposition);
  
  if (dpC1 || dpC2){
          dpGet(sDp1Split[1] + ".settings.v0", fActualV0);
          dpGet("NP02_DCS_01:" + sDp2Split[3] +"_" + sDp3Split[1] + ".settings.v0Decrease", fSettingsv0Decrease);
          dpGet(sDp1Split[1] + ".actual.isOn", fActualOn);
          if (fActualOn){
          dpSet(sDp1Split[1] + ".settings.v0", (fActualV0 - fSettingsv0Decrease));
          }
          
          if (refposition == "Top"){
            dpGet(sDpGroup, refgroup);
            dpGet(sDpCRP, refcrp);
            for (j=1; j<=nch; j++){
              dpGet(all_nomchannels[j] + ".ChargeReadoutPlane", crp);
              dpGet(all_nomchannels[j] + ".Group", group);
              dpGet(all_nomchannels[j] + ".Position", position);
              if(crp == refcrp && group == refgroup && position == "Bottom"){
                dpGet(all_channels[j] + ".settings.v0", fsettingV0);
                dpGet(all_channels[j] + ".actual.isOn", fActualOn);
                if (fActualOn){
                dpSet(all_channels[j] + ".settings.v0", fsettingV0 - fSettingsv0Decrease);
                }
              }
            }
          }   
          
          
     }

  
// Calculus of OvCRate
  
  if((iLength3 != 0) && ( iError3 != -1))
  {
    for(int k=1 ; k <= iLength3 ; k=k+1)
    {
      if (dbIntlk3[k])
      {
        iC3 = iC3 + 1;
      }
    }
  }
  
  fOvcRate = iC3/24.;
  
  dpSet(sDpRate, fOvcRate);

  
}


CaenIsOnCtrl(string dp1, bool status)
{

  dyn_string sDp1Split, sDp2Split, sDp3Split,sDp4Split, sDp5Split, sDp6Split, all_channels, all_nomchannels;
  string sDpPosition, sDpGroup, sDpCRP, sDpNominal;
  
  float fActualV0, fsettingV0, fNominalVal, fSettingsv0Decrease, refnominal;
  bool fActualOn;
  int nch, j;
  string position, refposition, crp, refcrp;
  int group, refgroup;

  all_channels = dpNames("NP02_DCS_01:CAEN/NP02-DCS-PS-LEM-01/board*/channel*", "FwCaenChannelA1580");
  all_nomchannels = dpNames("NP02_DCS_01:Board*Channel*", "CaenChannelConfig");
  
  nch = dynlen(all_channels);
  
  sDp1Split = strsplit(dp1, ".");
  sDp2Split = strsplit(dp1, "/");
  sDp3Split = strsplit(sDp2Split[4], ".");
  
  
  sDp4Split = strsplit(sDp1Split[1], "/");
  sDp5Split = strsplit(sDp4Split[3],"d");
  sDp6Split = strsplit(sDp4Split[4],"l");
                  
  ///"NP02_DCS_01:board00_channel000.actual.OvCRate"
  
  sDpPosition = "NP02_DCS_01:" + "Board" + sDp5Split[2] + "Channel" + sDp6Split[2] + ".Position";
  sDpGroup = "NP02_DCS_01:" + "Board" + sDp5Split[2] + "Channel" + sDp6Split[2] + ".Group";
  sDpCRP = "NP02_DCS_01:" + "Board" + sDp5Split[2] + "Channel" + sDp6Split[2] + ".ChargeReadoutPlane";
  sDpNominal = "NP02_DCS_01:" + "Board" + sDp5Split[2] + "Channel" + sDp6Split[2] + ".NominalValue";
  
  dpGet(sDpPosition, refposition);
  dpGet(sDpGroup, refgroup);
  dpGet(sDpCRP, refcrp);
  dpGet(sDpNominal, refnominal);
  
  if (refposition == "Top" && status == FALSE){
    DebugN("Switching off a top channel");
    for (j=1; j<=nch; j++)
    {
      dpGet(all_nomchannels[j] + ".ChargeReadoutPlane", crp);
      dpGet(all_nomchannels[j] + ".Group", group);
      dpGet(all_nomchannels[j] + ".Position", position);
      if(crp == refcrp && group == refgroup && position == "Bottom")
      {
         dpGet(all_channels[j] + ".settings.v0", fsettingV0);
         dpGet(all_channels[j] + ".actual.isOn", fActualOn);
         dpGet(all_nomchannels[j] +".NominalValue", fNominalVal);
         if (fActualOn){
         dpSet(all_channels[j] + ".settings.v0", fNominalVal - refnominal);
         }
      }
    }    
  }

}   
  
  
 
